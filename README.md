# Anleitung

Dieses Dokument soll erkären, wie Inhalt der Seite 
geändert werden können.

## Voraussetzungen

- [Markdown Syntax](https://www.markdownguide.org/basic-syntax)
- [GitLab Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/)
- [GitLab Merge Requset](https://docs.gitlab.com/ee/user/project/merge_requests/)

## Video Tutorial

In Arbeit

## Diskussionen

Fragen und Diskussionen außerhalb eines Merge Requests können über die [issues](https://gitlab.com/wir2020/wir2020.gitlab.io/-/issues) erfolgen.