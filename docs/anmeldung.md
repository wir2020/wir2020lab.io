# Mitglieder-Anmeldung

Mitglieder-Anmeldung mit folgenden Features:

- Captcha
- Double-Opt-in
- simpler DDOS Schutz
- Adressverifizierung
- Ausweis-Verifizierung

> HINWEIS: Es handelt sich um eine inoffizielle Anmeldung!

[zum Prototypen \(noch nicht verfügbar\)](#)