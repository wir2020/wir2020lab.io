# Landkreise

Für folgende Landkreise gibt es Telegram-Gruppe.  
Eine aktuelle Liste gibt es im Telegram Kanal [wir2020nds](https://t.me/wir2020nds).

- Ammerland
- [Aurich](https://t.me/widerstand2020LkAurichEmden)
- [Braunschweig](https://t.me/widerstand2020_Braunschweig)
- [Celle](https://t.me/widerstand2020Celle)
- [Cloppenburg](https://t.me/joinchat/Sr4bAUgaNB1iqZgk15ty9w)
- [Cuxhaven](https://t.me/Widerstand2020_Cuxland/3 )
- [Delmenhorst](https://t.me/joinchat/SeMjVksqprLY7ZXt0HBb7A)
- [Diepholz](https://t.me/joinchat/RJvoOkSIGKCJvq9hu9cXdA)
- [Emden](https://t.me/widerstand2020LkAurichEmden)
- Emsland
- [Friesland](https://t.me/widerstand2020fri)
- [Gifhorn](https://t.me/widerstand2020_Gifho)
- Goslar
- [Göttingen](https://t.me/widerstand2020Goettingen)
- Grafschaft Bentheim
- [Hameln-Pyrmont](https://t.me/widerstand2020_Hameln)
- [Hannover](https://t.me/Widerstand2020_Hannover)
- [Harburg](https://t.me/widerstand2020LK_Harburg)
- [Heidekreis](https://t.me/widerstand2020_HK)
- Helmstedt
- [Hildesheim](https://t.me/Widerstand2020_Hildesheim)
- [Holzminden](https://t.me/widerstand2020_Hameln)
- [Leer](https://t.me/joinchat/PKAyU0yfnAZY715zCM00Sw)
- Lüchow-Dannenberg
- [Lüneburg](https://t.me/joinchat/OEkqhR26K7BhISZRhbNcmQ)
- [Nienburg Weser](https://t.me/Widerstand2020_Nienburg)
- Northeim
- [Oldenburg](https://t.me/Widerstand2020Oldenburg)
- [Osnabrück](https://t.me/Widerstand2020_Osnabrueck)
- Osterholz
- Peine
- [Region Hannover](https://t.me/Widerstand2020_Hannover)
- [Rotenburg Wümme](https://t.me/Widerstand2020_ROW)
- [Salzgitter](https://t.me/gruppeWIR2020WFSZ)
- [Schaumburg](https://t.me/widerstand2020_Hameln)
- [Stade](https://t.me/stadewir2020)
- [Uelzen](https://t.me/widerstand2020_uelz)
- [Vechta](https://t.me/joinchat/Sr4bAUgaNB1iqZgk15ty9w)
- [Verden](https://t.me/joinchat/SJhia1a_eR87dJOODII7UQ)
- Wesermarsch
- Wilhelmshaven
- Wittmund
- [Wolfenbüttel](https://t.me/gruppeWIR2020WFSZ)
- [Wolfsburg](https://t.me/Widerstand2020_Wob)